var httpntlm = require('httpntlm'),
request = require('request'),
fs = require('fs'),
xml2js = require('xml2js');
var Promise = require('bluebird');
var parser = new xml2js.Parser();
var parseString = xml2js.parseString;
var builder = new xml2js.Builder();

var ews = function () {
that = this;
that.constants = {
    CalendarBusyStatus: {
        Free: 'Free',
        Tentative: 'Tentative',
        Busy: 'Busy',
        OutOfOffice: 'OOF',
        NoStatus: 'NoData',
        WorkingElsewhere: 'WorkingElsewhere'
    }
};

that.configObj = {};
that.config = function (username, password, url, domain) {
    that.configObj.username = username;
    that.configObj.password = password;
    that.configObj.url = url;
    that.configObj.domain = domain;
};
that.delete = function (contents, callback) {
    httpntlm.post({
        url: that.configObj.url,
        username: that.configObj.username,
        password: that.configObj.password,
        domain: that.configObj.domain,
        body: contents,
        headers: {
            "content-type": "text/xml",
            "cache-control": "no-cache"
        }
    }, function (err, res) {
        if (err) return err;
        if (res.statusCode != 200) {
            console.log('Error with status code: ' + res.statusCode);
        } else {
            console.log("Success");
            callback();
        }
    });
}
that.post = function (contents, callback) {
    httpntlm.post({
        url: that.configObj.url,
        username: that.configObj.username,
        password: that.configObj.password,
        domain: that.configObj.domain,
        body: contents,
        headers: {
            "content-type": "text/xml",
            "cache-control": "no-cache"
        }
    }, function (err, res) {
        if (err) return err;
        if (res.statusCode != 200) {
            console.log('Error with status code: ' + res.statusCode);
        } else {
            console.log("Success");
            var re = new RegExp("<t:CalendarItem>(.*?)</t:CalendarItem?>", "gmi");
            while (res = re.exec(res.body)) {
                var id = res[1].substring(res[1].lastIndexOf("<t:ItemId Id=") + 1, res[1].lastIndexOf("ChangeKey"));
                id = id.replace('t:ItemId Id=', '');
                id = id.replace(' ', '');
                id = id.replace(/['"]+/g, '');
                var changeKey = res[1].substring(res[1].lastIndexOf("Key=") + 1, res[1].lastIndexOf("/>"));
                changeKey = changeKey.replace(/['"]+/g, '');
                changeKey = changeKey.replace('ey=', '');
                callback({ id: id, changeKey: changeKey });
            }
        }
    });
}
that.requestFolder = function (contents, callback) {
    httpntlm.post({
        url: that.configObj.url,
        username: that.configObj.username,
        password: that.configObj.password,
        domain: that.configObj.domain,
        body: contents,
        headers: {
            "content-type": "text/xml",
            "cache-control": "no-cache"
        }
    }, function (err, res) {
        if (err) return err;
        if (res.statusCode != 200) {
            console.log('Error with status code: ' + res.statusCode);
        } else {
            console.log("Success");
            var re = new RegExp("<t:CalendarFolder>(.*?)</t:CalendarFolder?>", "gmi");
            while (res = re.exec(res.body)) {
                var id = res[1].substring(res[1].lastIndexOf("<t:FolderId Id=") + 1, res[1].lastIndexOf("ChangeKey"));
                id = id.replace(' ', '');
                id = id.replace(/['"]+/g, '');
                id = id.replace('t:FolderIdId=', '');
                var changeKey = res[1].substring(res[1].lastIndexOf("Key=") + 1, res[1].lastIndexOf("/>"));
                changeKey = changeKey.replace(/['"]+/g, '');
                changeKey = changeKey.replace('ey=', '');
                callback({ id: id, changeKey: changeKey });
            }
        }
    });
},
    that.requestEvents = function (contents, callback) {
        httpntlm.post({
            url: that.configObj.url,
            username: that.configObj.username,
            password: that.configObj.password,
            domain: that.configObj.domain,
            body: contents,
            headers: {
                "content-type": "text/xml",
                "cache-control": "no-cache"
            }
        }, function (err, res) {
            if (err) return err;
            if (res.statusCode != 200) {
                console.log(res);
                console.log('Error with status code: ' + res.statusCode);
            } else {
                console.log("Success");
                parseString(res.body, function (err, res) {
                    callback(res['s:Envelope']['s:Body'][0]['m:FindItemResponse'][0]['m:ResponseMessages'][0]['m:FindItemResponseMessage'][0]['m:RootFolder'][0]['t:Items'][0]['t:CalendarItem']);
                });
            }
        });
    }
that.requestContactsList = function (contents, callback) {
    httpntlm.post({
        url: that.configObj.url,
        username: that.configObj.username,
        password: that.configObj.password,
        domain: that.configObj.domain,
        body: contents,
        headers: {
            "content-type": "text/xml",
            "cache-control": "no-cache"
        }
    }, function (err, res) {
        if (err) return err;
        if (res.statusCode != 200) {
            console.log(err);
            console.log('Error with status code: ' + res.statusCode);
        } else {
            parseString(res.body, function (err, res) {
                callback(res);
            });
        }
    });
}
this.createTask = function (subject, dueDate) {
    var contents = fs.readFileSync(__dirname + '/xmlFiles/createTask.xml').toString();
    contents = contents.replace("_SUBJECT_", subject);
    contents = contents.replace("_DUE_DATE_", dueDate);
    that.post(contents);
};

this.createAppointment = function (subject, body, startTime, endTime, busyStatus, location) {
    return new Promise(function (resolve, reject) {
        var contents = fs.readFileSync(__dirname + '/xmlFiles/createAppointment.xml').toString();
        contents = contents.replace("_SUBJECT_", subject);
        contents = contents.replace("_BODY_", body);
        contents = contents.replace("_START_DATE_", startTime);
        contents = contents.replace("_END_DATE_", endTime);
        contents = contents.replace("_STATUS_", busyStatus);
        contents = contents.replace("_LOCATION_", location);
        that.post(contents, function (resp) {
            resolve(resp);
        });
    })
}
this.deleteAppointment = function (id, key) {
    return new Promise(function (resolve, reject) {
        var contents = fs.readFileSync(__dirname + '/xmlFiles/deleteAppointment.xml').toString();
        contents = contents.replace("_ID_", id);
        contents = contents.replace("_KEY_", key);
        that.delete(contents, function (resp) {
            resolve(resp);
        });
    })
}
this.sendMail = function (csvTo, subject, body) {
    var contents = fs.readFileSync(__dirname + '/xmlFiles/sendMail.xml').toString();
    contents = contents.replace("_TO_", csvTo);
    contents = contents.replace("_SUBJECT_", subject);
    contents = contents.replace("_BODY_", body);
    that.post(contents);
}
this.getAvailabilities = function (email,start, end) {
    return new Promise(function (resolve, reject) {
        var contents = fs.readFileSync(__dirname + '/xmlFiles/getFolder.xml').toString();
        contents = contents.replace("_ID_", "calendar");
        that.requestFolder(contents, function (response) {
            if (response.id.length > 0 && response.changeKey.length > 0) {
                contents = fs.readFileSync(__dirname + '/xmlFiles/getEvents.xml').toString();
                contents = contents.replace("_EMAIL_", email);
                contents = contents.replace("_START_", start);
                contents = contents.replace("_END_", end);
                contents = contents.replace("_ID_", response.id);
                contents = contents.replace("_KEY_", response.changeKey);
                that.requestEvents(contents, function (response) {
                    resolve(response);
                })

            } else {
                resolve([]);
            }
        });
    })
},
    this.getContactsList = function (local) {
        return new Promise(function (resolve, reject) {
            var contents = fs.readFileSync(__dirname + '/xmlFiles/getContactsList.xml').toString();
            contents = contents.replace("_LOCAL_", local);
            that.requestContactsList(contents, function (response) {
                resolve(response);
            });
        })
    }
}

module.exports = new ews();